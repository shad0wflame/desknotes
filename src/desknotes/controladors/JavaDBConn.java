/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desknotes.controladors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PryonX_2
 */
public class JavaDBConn {
    private Connection conn = null;
    public Connection CreateDB(){
       try{
         Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
         conn = DriverManager.getConnection("jdbc:derby:.\\DB\\Derby.DB;create=true");
         if (conn != null){
            String CreateTable = "CREATE TABLE Notes(ID int NOT NULL, Text varchar(300), X int NOT NULL, Y int NOT NULL, Color1 varchar(20) NOT NULL, Color2 varchar(20) NOT NULL, PRIMARY KEY(ID))";
            try {
                try (PreparedStatement pstm = conn.prepareStatement(CreateTable)) {
                    pstm.execute();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getLocalizedMessage());
            }
        }
      }catch(SQLException | ClassNotFoundException e){
       System.out.println(e.getMessage());
      }
       return conn;
    }
    
    public Connection ConnectDB(){
        try{
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            conn = DriverManager.getConnection("jdbc:derby:.\\DB\\Derby.DB");
        } catch(SQLException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
       return conn;
    }
     
    public void CloseDB(){
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(JavaDBConn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Object[][] getNotes() throws SQLException{
        String query = "SELECT * FROM Notes";
        String query2 = "SELECT count(*) as total FROM Notes";
        int i = 0, n = 0;
        Connection con = this.ConnectDB();
        
        //GET NUMBER OF NOTES.
        Statement selectStmt2 = con.createStatement();
        try (ResultSet rs2 = selectStmt2.executeQuery(query2)) {
            rs2.next();
            n = rs2.getInt("total");
            rs2.close();
        }
        
        //GET THE ACTUAL DATA FROM THE NOTES.
        Statement selectStmt = con.createStatement();
        ResultSet rs = selectStmt.executeQuery(query);
        Object[][] Data = new String [n][6];
        while(rs.next()){
            Data[i][0] = rs.getString(1);
            Data[i][1] = rs.getString(2);
            Data[i][2] = rs.getString(3);
            Data[i][3] = rs.getString(4);
            Data[i][4] = rs.getString(5);
            Data[i][5] = rs.getString(6);
            i++;
        }
        selectStmt.close();
        selectStmt2.close();
        rs.close();
        con.close();
        return Data;
    }
        
    public int MaxID() throws SQLException{
        Connection con = this.ConnectDB();
        Statement selectStmt = con.createStatement();
        ResultSet rs = selectStmt.executeQuery("SELECT MAX(ID) FROM Notes");
        rs.next();
        int id = rs.getInt(1);
        selectStmt.close();
        rs.close();
        con.close();
        return id;
    }
    
    public void newNote(int id, int x, int y) throws SQLException {
        try (Connection con = this.ConnectDB(); Statement stmt = con.createStatement()) {
            stmt.execute("INSERT INTO Notes (ID,Text,X,Y,Color1,Color2) VALUES ("+id+",'',"+x+","+y+",'153,255,153','204,255,204')");
            stmt.close();
            con.close();
        }
    }
    
    public void uptateNote(int id, String text, int x, int y, String[] color1, String[] color2) throws SQLException{
        try (Connection con = this.ConnectDB(); Statement stmt = con.createStatement()) {
            String sql = "UPDATE Notes " +
                    "SET text = '"+text+"', X = "+x+", Y = "+y+", Color1 = '"+color1[0]+","+color1[1]+","+color1[2]+"', Color2 = '"+color2[0]+","+color2[1]+","+color2[2]+"' WHERE id = "+id+"";
            stmt.executeUpdate(sql);
            stmt.close();
            con.close();
        }
    }
    
    public void deleteNote(int id) throws SQLException{
        try (Connection con = this.ConnectDB(); Statement stmt = con.createStatement()) {
            stmt.execute("DELETE FROM Notes WHERE ID = "+id+"");
            stmt.close();
            con.close();
        }
    }
}
